The Law Offices of Joseph M. Lichtenstein, PC represents injured victims or families that have lost a loved one in all types of claims resulting from medical negligence.

Address: 170 Old Country Road, Mineola, NY 11501, USA

Phone: 516-873-6300

Website: https://www.themedicalattorney.com
